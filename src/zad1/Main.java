/**
 *
 *  @author Kwiatek Piotr S17188
 *
 */

package zad1;


import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class Main extends Application {

  private static Service s;
  private static String weatherJson;
  private static Double rate1;
  private static Double rate2;

  public static void main(String[] args) throws IOException, JSONException {
    s = new Service("Poland");
    weatherJson = s.getWeather("Warsaw");
    rate1 = s.getRateFor("USD");
    rate2 = s.getNBPRate();
    // ...
    // część uruchamiająca GUI

    Application.launch(args);
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    Group root = new Group();
    GridPane gridPane = new GridPane();
    gridPane.setPadding(new Insets(10, 10, 10, 10));
    gridPane.setMinSize(400, 600);

    Button getWeatherBtn = new Button("Pobierz pogode");
    Button getRateBtn = new Button("Pobierz kurs waluty");
    Button getRateNbpBtn = new Button("Pobierz kurs waluty NBP");
    Button getWikiBtn = new Button("Wyświetl stronę Wiki");

    TextArea textArea = new TextArea();
    textArea.setPrefHeight(600);
    textArea.setWrapText(true);

    gridPane.add(getWeatherBtn, 1, 0, 1, 1);
    gridPane.add(getRateBtn, 2, 0, 1, 1);
    gridPane.add(getRateNbpBtn, 3, 0, 1, 1);
    gridPane.add(getWikiBtn, 4, 0, 1, 1);
    gridPane.add(textArea, 0, 1, 5, 5);

    Scene scene = new Scene(gridPane);
    Stage stage = new Stage();
    stage.setTitle("Program");
    stage.setScene(scene);
    stage.show();

    getWeatherBtn.setOnAction(event -> {
//      try {
//        textArea.setText(ParseWeather(weatherJson));
//      } catch (JSONException e) {
//        e.printStackTrace();
//      }
      textArea.setText(weatherJson);
    });

    getRateBtn.setOnAction(event -> {
      if(rate1 != null){
        textArea.setText(String.valueOf(rate1));
      }else{
        textArea.setText("Dla danego kraju nie ma danych w bazie");
      }
    });

    getRateNbpBtn.setOnAction(event -> {
      if(rate2 != null){
        textArea.setText(String.valueOf(rate2));
      }else{
        textArea.setText("Dla danego kraju nie ma danych w bazie.");
      }
    });

    getWikiBtn.setOnAction(event -> {
      WebView webView = new WebView();

      WebEngine webEngine = webView.getEngine();
      webEngine.load(s.getWikiCity());

      Scene sceneWeb = new Scene(webView);
      Stage stageWeb = new Stage();
      stageWeb.setTitle("Web");
      stageWeb.setScene(sceneWeb);
      stageWeb.show();
    });

  }

//  public String ParseWeather(String json) throws JSONException {
//    JSONObject jsonObject = new JSONObject(json);
//
//    String weatherData = "";
//
//    weatherData += "Temperatura: " + jsonObject.getJSONObject("main").get("temp") + "\n";
//    weatherData += "Temperatura odczuwalna: " + jsonObject.getJSONObject("main").get("feels_like") + "\n";
//    weatherData += "Ciśnienie: " + jsonObject.getJSONObject("main").get("pressure") + "\n";
//    weatherData += "Wilgotność: " + jsonObject.getJSONObject("main").get("humidity") + "\n";
//
//    return weatherData;
//
//  }
}
