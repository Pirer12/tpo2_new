/**
 *
 *  @author Kwiatek Piotr S17188
 *
 */

package zad1;


import net.aksingh.owmjapis.CurrentWeather;
import net.aksingh.owmjapis.OpenWeatherMap;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.*;

public class Service {
    String kraj;
    String miasto;
    Map<String, String> waluty = getAvailableCurrencies();
    Map<String, String> krajeISO = getAvailableCountriesISO();

    public Service(String kraj) {
        this.kraj = kraj;
    }

    public String getWeather(String miasto) throws IOException, JSONException {
        this.miasto = miasto;
//        for (String country : krajeISO.keySet()) {
//            String currencyCode = krajeISO.get(country);
//            System.out.println(country + " => " + currencyCode);
//        }

        OpenWeatherMap owm = new OpenWeatherMap(OpenWeatherMap.Units.METRIC, "e153b6f6e93ae11006a6912da80e280d");
        CurrentWeather cwd = owm.currentWeatherByCityName(miasto,krajeISO.get(this.kraj));

        boolean hasStation = cwd.hasBaseStation();
//          boolean hasStation = false;
//        System.out.println(cwd.hasBaseStation());

        if(hasStation){
//            return "asd";
            return cwd.getRawResponse();
        }else{
//            return "{\"visibility\":10000,\"timezone\":3600,\"main\":{\"temp\":-2.31,\"temp_min\":-4.44,\"humidity\":58,\"pressure\":1028,\"feels_like\":-6.79,\"temp_max\":0},\"clouds\":{\"all\":0},\"sys\":{\"country\":\"PL\",\"sunrise\":1584161518,\"sunset\":1584203899,\"id\":1713,\"type\":1},\"dt\":1584222537,\"coord\":{\"lon\":21.01,\"lat\":52.23},\"weather\":[{\"icon\":\"01n\",\"description\":\"clear sky\",\"main\":\"Clear\",\"id\":800}],\"name\":\"Warsaw\",\"cod\":200,\"id\":756135,\"base\":\"stations\",\"wind\":{\"deg\":300,\"speed\":2.1}}";
            return "Podane miasto nie istnieje w bazie lub nie ma stacji pogodowej.";
        }
    }

    public Double getRateFor(String kod_waluty) throws IOException, JSONException {
        String krajForRate = waluty.get(kraj);
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet data = new HttpGet("https://api.exchangeratesapi.io/latest?base=" + kod_waluty + "&symbols=" + krajForRate);

        CloseableHttpResponse response = httpClient.execute(data);

        HttpEntity entity = response.getEntity();

        if (entity != null){
            String result = EntityUtils.toString(entity);
            if(!result.contains("error")){
                JSONObject jsonObject = new JSONObject(result);
                return (double)jsonObject.getJSONObject("rates").get(krajForRate);
            }else{
                return null;
            }
        }else{
            return null;
        }
    }

    public Double getNBPRate() throws IOException {
        String[] nbpPages = new String[]{"http://www.nbp.pl/kursy/kursya.html", "http://www.nbp.pl/kursy/kursyb.html"};
        String result = "";
        CloseableHttpClient httpClient = HttpClients.createDefault();

        for(String x: nbpPages){
            HttpGet data = new HttpGet(x);
            CloseableHttpResponse response = httpClient.execute(data);
            HttpEntity entity = response.getEntity();

            result += EntityUtils.toString(entity);
        }

        int indeksKoduWaluty = result.indexOf(waluty.get(kraj));

        if(indeksKoduWaluty != -1){
            int indeksKursu = result.indexOf(">", indeksKoduWaluty + 10) + 1;
            Double kursWaluty = Double.valueOf(result.substring(indeksKursu, indeksKursu + 6).replace(',', '.'));

            return kursWaluty;
        }else{
            return null;
        }
    }

    public String getWikiCity(){
        String result = "https://en.wikipedia.org/wiki/" + this.miasto;

        return result;
    }

    private Map<String, String> getAvailableCurrencies() {
        Locale[] locales = Locale.getAvailableLocales();
        Map<String, String> currencies = new TreeMap<>();
        for (Locale locale : locales) {
            try {
                currencies.put(locale.getDisplayCountry(Locale.ENGLISH),
                        Currency.getInstance(locale).getCurrencyCode());
            } catch (Exception e) {

            }
        }
        return currencies;
    }

    private Map<String, String> getAvailableCountriesISO() {
        Locale[] locales = Locale.getAvailableLocales();
        Map<String, String> isoMap = new TreeMap<>();
        for (Locale locale : locales) {
            try {
                isoMap.put(locale.getDisplayCountry(Locale.ENGLISH),
                        locale.getCountry());
            } catch (Exception e) {

            }
        }
        return isoMap;
    }
}
